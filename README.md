#  Marvel App
===============================

Marvel App is an application that lists Marvel characters and their respective comic book entries. 

## Architecture
This project uses MVVM architectural pattern.

### Dependencies ###

* Android Support Tools (recyclerView, cardView, vector,... ) v27.1.1
* ButterKnife v8.8.1
* Picasso v2.5.2
* jUnit v4.12
* Android Support Test v1.0.2
* Mockito v1.10.19
* Robolectric v3.8
* Espresso v3.0.2

### Important Notes ###

The application includes three Activities: Splash, Main and Detail Activities. The Main Activity is a list o characters available in the Marvel API. In Detail Activity it is possible to check the description of the selected character as well as his comic participations.

Some simple Test Cases was designed to test application UI functionality and core classes using jUnit and AndroidUnitTest.

## UX/UI
The UI/UX was created by Flavia Ampessan.

### Screenshots:

#### Splash Screen


![Splash Screen](https://bitbucket.org/mausanfre/msfmarvelapp/raw/2fc4578b186e68443e3f2ed87a58c8ac0e9d3de4/Screenshots/Screenshot1.png "Splash Screen")

#### Characters List

![Characters List](https://bitbucket.org/mausanfre/msfmarvelapp/raw/2fc4578b186e68443e3f2ed87a58c8ac0e9d3de4/Screenshots/Screenshot2.png "Characters List")

#### Character Detail

![Character Detail](https://bitbucket.org/mausanfre/msfmarvelapp/raw/2fc4578b186e68443e3f2ed87a58c8ac0e9d3de4/Screenshots/Screenshot3.png "Character Detail")

#### Comics List

![Comics List](https://bitbucket.org/mausanfre/msfmarvelapp/raw/2fc4578b186e68443e3f2ed87a58c8ac0e9d3de4/Screenshots/Screenshot4.png "Comics List")

