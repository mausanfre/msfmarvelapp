package br.com.softdesign.marvelapp.viewmodel;

import android.databinding.BindingAdapter;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import br.com.softdesign.marvelapp.model.Results;
import br.com.softdesign.marvelapp.viewmodel.base.BaseViewModel;

public class ComicsViewModel extends BaseViewModel {

    private String name;
    private String description;
    private String pictureUrl;

    public ComicsViewModel(String name, String description, String pictureUrl) {
        this.name = name;
        this.description = description;
        this.pictureUrl = pictureUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    @BindingAdapter({"bind:imageUrl"})

    public static void loadImage(ImageView imageView, String imageUrl){
        Picasso.get().load(imageUrl).fit().into(imageView);
    }

    public String getImageUrl() {
        return pictureUrl;
    }

    public static List<ComicsViewModel> toViewModelList(List<Results> resultsList) {
        List<ComicsViewModel> viewModelList = new ArrayList<ComicsViewModel>();

        for (Results results : resultsList) {
            viewModelList.add(
                    new ComicsViewModel(
                            results.getTitle(),
                            results.getDescription(),
                            results.getThumbnail().getPath() + "." + results.getThumbnail().getExtension()
                    )
            );
        }
        return viewModelList;
    }

}
