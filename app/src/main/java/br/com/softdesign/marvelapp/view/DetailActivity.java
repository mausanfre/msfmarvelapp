package br.com.softdesign.marvelapp.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TypefaceSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.softdesign.marvelapp.R;
import br.com.softdesign.marvelapp.viewmodel.CharacterViewModel;
import br.com.softdesign.marvelapp.viewmodel.ComicsViewModel;
import br.com.softdesign.marvelapp.viewmodel.DetailViewModel;
import br.com.softdesign.marvelapp.viewmodel.base.LoadDataCallbacks;
import br.com.softdesign.marvelapp.databinding.DetailCharacterBinding;

public class DetailActivity extends AppCompatActivity implements LoadDataCallbacks {

    private static final String TAG = DetailActivity.class.getSimpleName();

    public static final String CHARACTER_ITEM_DATA = "CHARACTER_ITEM_DATA";

    private boolean loading = true;

    ComicsAdapter adapter;
    RecyclerView recyclerView;
    ConstraintLayout backConstraintLayout;
    ImageView pictureImageView;
    TextView nameTextView;
    TextView descriptionTextView;
    View divisorView;
    TextView comicsTextView;
    NestedScrollView nestedScrollView;
    DetailViewModel detailViewModel;
    ProgressBar comicsProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CharacterViewModel characterViewModel = (CharacterViewModel)this.getIntent().getSerializableExtra(CHARACTER_ITEM_DATA);
        detailViewModel = new DetailViewModel(characterViewModel);
        detailViewModel.addListener(this);

        final DetailCharacterBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_detail);
        binding.setDetailCharacterViewModel(detailViewModel);

        SpannableString s = new SpannableString(characterViewModel.getName());
        s.setSpan(new TypefaceSpan("@font/barlowcondensed_regular"), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        setTitle(s);

        recyclerView = findViewById(R.id.comicsRecyclerView);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);

        nestedScrollView = findViewById(R.id.nestedScrollView);
        nestedScrollView.setOnScrollChangeListener(getScrollChangeListener());

        pictureImageView = findViewById(R.id.pictureImageView);
        nameTextView = findViewById(R.id.nameTextView);
        descriptionTextView = findViewById(R.id.descriptionTextView);

        backConstraintLayout = findViewById(R.id.backConstraintLayout);

        divisorView = findViewById(R.id.divisorView);
        comicsTextView = findViewById(R.id.comicsTextView);
        comicsProgressBar = findViewById(R.id.comicsProgressBar);

        divisorView.setVisibility(View.GONE);
        comicsTextView.setVisibility(View.GONE);

        backConstraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        adapter = new ComicsAdapter(detailViewModel.getComicsList());
        recyclerView.setAdapter(getComicsAdapter(detailViewModel.getComicsList()));

        comicsProgressBar.post(new Runnable() {
            @Override
            public void run() {
                comicsProgressBar.setVisibility(View.VISIBLE);
            }
        });
        detailViewModel.loadComics(getContext(), false);

    }

    public Context getContext() {
        return this;
    }

    @NonNull
    ComicsAdapter getComicsAdapter(List<ComicsViewModel> mData){
        ComicsAdapter comicsAdapter = new ComicsAdapter(mData);
        return comicsAdapter;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @NonNull
    private NestedScrollView.OnScrollChangeListener getScrollChangeListener() {
        return new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) && scrollY > oldScrollY) {
                        int visibleItemCount = recyclerView.getLayoutManager().getChildCount();
                        int totalItemCount = recyclerView.getLayoutManager().getItemCount();
                        int pastVisiblesItems = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();

                        if (loading) {
                            if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                                comicsProgressBar.setVisibility(View.VISIBLE);
                                detailViewModel.loadComics(getContext(),true);
                            }
                        }
                    }
                }
            }
        };
    }

    @Override
    public void onDataLoaded(boolean nextPage) {

        recyclerView.getAdapter().notifyDataSetChanged();
        divisorView.setVisibility(detailViewModel.getComicsList().size() > 0 ? View.VISIBLE : View.GONE);
        comicsTextView.setVisibility(detailViewModel.getComicsList().size() > 0 ? View.VISIBLE : View.GONE);
        comicsProgressBar.setVisibility(View.GONE);

    }

    @Override
    public void onError(Exception error) {

        Toast.makeText(getBaseContext(), R.string.mensagem_problema_acesso_servico, Toast.LENGTH_SHORT).show();
        comicsProgressBar.setVisibility(View.GONE);

    }

    @Override
    public void onNoDataLoaded() {
        comicsProgressBar.setVisibility(View.GONE);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
