package br.com.softdesign.marvelapp.model;

import java.math.BigInteger;

import br.com.softdesign.marvelapp.model.base.BaseModel;

public class Data extends BaseModel {

    private BigInteger total;

    private Results[] results;

    private BigInteger offset;

    public BigInteger getTotal ()
    {
        return total;
    }

    public void setTotal (BigInteger total)
    {
        this.total = total;
    }

    public Results[] getResults ()
    {
        return results;
    }

    public void setResults (Results[] results)
    {
        this.results = results;
    }

    public BigInteger getOffset ()
    {
        return offset;
    }

    public void setOffset (BigInteger offset)
    {
        this.offset = offset;
    }

}