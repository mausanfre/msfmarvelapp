package br.com.softdesign.marvelapp.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.softdesign.marvelapp.databinding.CharacterBinding;
import br.com.softdesign.marvelapp.viewmodel.CharacterViewModel;

import java.util.List;

public class CharacterAdapter extends RecyclerView.Adapter<CharacterAdapter.ViewHolder> {

    private List<CharacterViewModel> characterList;
    private LayoutInflater layoutInflater;
    private ItemClickListener mClickListener;

    CharacterAdapter(List<CharacterViewModel> data) {
        this.characterList = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(layoutInflater == null){
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        CharacterBinding characterBinding = CharacterBinding.inflate(layoutInflater, parent, false);
        return new ViewHolder(characterBinding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CharacterViewModel viewModel = characterList.get(position);
        holder.bind(viewModel);
    }

    @Override
    public int getItemCount() {
        return characterList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private CharacterBinding characterBinding;

        ViewHolder(CharacterBinding characterBinding) {
            super(characterBinding.getRoot());
            itemView.setOnClickListener(this);
            this.characterBinding = characterBinding;
        }

        public void bind(CharacterViewModel viewModel){
            this.characterBinding.setCharacterViewModel(viewModel);
        }

        public CharacterBinding getCharacterBinding() {
            return characterBinding;
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    CharacterViewModel getItem(int id) {
        return characterList.get(id);
    }

    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

}
