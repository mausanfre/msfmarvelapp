package br.com.softdesign.marvelapp.model;

import java.math.BigInteger;

import br.com.softdesign.marvelapp.model.base.BaseModel;

public class Comics extends BaseModel {

    private String collectionURI;

    public String getCollectionURI ()
    {
        return collectionURI;
    }

    public void setCollectionURI (String collectionURI)
    {
        this.collectionURI = collectionURI;
    }


}
