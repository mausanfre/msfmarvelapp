package br.com.softdesign.marvelapp.view;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TypefaceSpan;
import android.view.View;
import android.widget.Toast;

import java.util.List;

import br.com.softdesign.marvelapp.R;
import br.com.softdesign.marvelapp.viewmodel.CharacterViewModel;
import br.com.softdesign.marvelapp.viewmodel.MainViewModel;
import br.com.softdesign.marvelapp.viewmodel.base.LoadDataCallbacks;

public class MainActivity extends AppCompatActivity implements LoadDataCallbacks {

    private static final String TAG = MainActivity.class.getSimpleName();

    RecyclerView recyclerView;
    CharacterAdapter adapter;
    SwipeRefreshLayout swipeRefreshLayout;
    MainViewModel mainViewModel;

    private boolean loadingData = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainViewModel = new MainViewModel(getApplicationContext());
        mainViewModel.addListener(this);

        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(getOnSwipeRefreshListener());

        recyclerView = findViewById(R.id.characterRecyclerView);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addOnScrollListener(getCharacterScrollListener());

        SpannableString s = new SpannableString(getResources().getString(R.string.characters_title));
        s.setSpan(new TypefaceSpan("@font/barlowcondensed_regular"), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        setTitle(s);

        adapter = new CharacterAdapter(mainViewModel.getCharactersList());
        recyclerView.setAdapter(getCharacterAdapter(mainViewModel.getCharactersList()));

        swipeRefreshLayout.setEnabled(true);
        loadingData = true;
        mainViewModel.loadCharacters(getContext(), false);

    }

    @NonNull
    CharacterAdapter getCharacterAdapter(List<CharacterViewModel> mData){
        CharacterAdapter characterAdapter = new CharacterAdapter(mData);

        characterAdapter.setClickListener(new CharacterAdapter.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                CharacterViewModel character = adapter.getItem(position);

                Intent intent = new Intent(getBaseContext(), DetailActivity.class);
                intent.putExtra(DetailActivity.CHARACTER_ITEM_DATA, character);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        return characterAdapter;
    }

    @NonNull
    private RecyclerView.OnScrollListener getCharacterScrollListener() {
        return new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (!loadingData && dy > 0) //check for scroll down
                {
                    int visibleItemCount = recyclerView.getLayoutManager().getChildCount();
                    int totalItemCount = recyclerView.getLayoutManager().getItemCount();
                    int pastVisiblesItems = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();

                    if (mainViewModel.isLoading()) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount-5) {
                            swipeRefreshLayout.post(new Runnable() {
                                @Override
                                public void run() {
                                    swipeRefreshLayout.setRefreshing(true);
                                }
                            });
                            loadingData = true;
                            mainViewModel.loadCharacters(getContext(), true);
                        }
                    }
                }
            }
        };
    }

    @NonNull
    private SwipeRefreshLayout.OnRefreshListener getOnSwipeRefreshListener() {
        return new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mainViewModel.loadCharacters(getContext(),false);
            }
        };
    }

    @Override
    public void onDataLoaded(boolean nextPage) {

        recyclerView.getAdapter().notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);
        loadingData = false;

    }

    @Override
    public void onError(Exception error) {

        Toast.makeText(getBaseContext(), R.string.mensagem_problema_acesso_servico, Toast.LENGTH_SHORT).show();
        swipeRefreshLayout.setRefreshing(false);
        loadingData = false;

    }

    @Override
    public void onNoDataLoaded() {
        swipeRefreshLayout.setRefreshing(false);
        loadingData = false;

    }

    public Context getContext() {
        return this;
    }
}
