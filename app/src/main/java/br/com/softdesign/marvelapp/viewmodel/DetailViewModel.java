package br.com.softdesign.marvelapp.viewmodel;

import android.content.Context;
import android.support.annotation.NonNull;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.softdesign.marvelapp.model.ServiceData;
import br.com.softdesign.marvelapp.service.CharacterService;
import br.com.softdesign.marvelapp.viewmodel.base.BaseViewModel;
import br.com.softdesign.marvelapp.viewmodel.base.LoadDataCallbacks;

public class DetailViewModel extends CharacterViewModel {

    private List<LoadDataCallbacks> listeners = new ArrayList<LoadDataCallbacks>();

    private boolean loading = true;
    private long comicsTotalData = 0;
    private List<ComicsViewModel> comicsList;

    public DetailViewModel(String name, String description, String pictureUrl, String comicsUrl) {
        super(name, description, pictureUrl, comicsUrl);
        comicsList = new ArrayList<>();
    }

    public DetailViewModel(CharacterViewModel characterViewModel) {
        super(characterViewModel.getName(), characterViewModel.getDescription(), characterViewModel.getPictureUrl(), characterViewModel.getComicsUrl());
        comicsList = new ArrayList<>();
    }

    public void addListener(LoadDataCallbacks toAdd) {
        listeners.add(toAdd);
    }

    public List<ComicsViewModel> getComicsList() {
        return comicsList;
    }

    public long getComicsTotalData() {
        return comicsTotalData;
    }

    public void loadComics(Context context, boolean nextPage) {

        if (nextPage && comicsList.size() >= getComicsTotalData()) {
            for (LoadDataCallbacks ld : listeners)
                ld.onNoDataLoaded();
            return;
        }

        loading = false;
        Integer offset = nextPage && comicsList != null && comicsList.size() > 0 ? comicsList.size() : 0;
        CharacterService.getComics(
                context,
                offset.toString(),
                null,
                this.getComicsUrl(),
                getOnResponseServiceData(nextPage),
                getOnErrorServiceData()
        );
    }

    @NonNull
    private Response.Listener<ServiceData> getOnResponseServiceData(final boolean nextPage) {
        return new Response.Listener<ServiceData>() {
            @Override
            public void onResponse(ServiceData response) {
                List<ComicsViewModel> newDataList = ComicsViewModel.toViewModelList(Arrays.asList(response.getData().getResults()));
                comicsTotalData = response.getData().getTotal().longValue();

                if (!nextPage)
                    comicsList.clear();

                comicsList.addAll(newDataList);
                for (LoadDataCallbacks ld : listeners)
                    ld.onDataLoaded(nextPage);
                loading = true;
            }
        };
    }

    @NonNull
    private Response.ErrorListener getOnErrorServiceData() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                for (LoadDataCallbacks ld : listeners)
                    ld.onError(error);
            }
        } ;
    }

}
