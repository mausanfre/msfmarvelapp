package br.com.softdesign.marvelapp.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.softdesign.marvelapp.databinding.ComicsBinding;
import br.com.softdesign.marvelapp.viewmodel.ComicsViewModel;

import java.util.List;

public class ComicsAdapter extends RecyclerView.Adapter<ComicsAdapter.ViewHolder> {

    private List<ComicsViewModel> comicsList;
    private LayoutInflater layoutInflater;
    private ItemClickListener mClickListener;

    ComicsAdapter(List<ComicsViewModel> data) {
        this.comicsList = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(layoutInflater == null){
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        ComicsBinding comicsBinding = ComicsBinding.inflate(layoutInflater, parent, false);
        return new ViewHolder(comicsBinding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ComicsViewModel viewModel = comicsList.get(position);
        holder.bind(viewModel);
    }

    @Override
    public int getItemCount() {
        return comicsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ComicsBinding comicsBinding;

        ViewHolder(ComicsBinding comicsBinding) {
            super(comicsBinding.getRoot());
            itemView.setOnClickListener(this);
            this.comicsBinding = comicsBinding;
        }

        public void bind(ComicsViewModel viewModel){
            this.comicsBinding.setComicsViewModel(viewModel);
        }

        public ComicsBinding getComicsBinding() {
            return comicsBinding;
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    ComicsViewModel getItem(int id) {
        return comicsList.get(id);
    }

    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

}
