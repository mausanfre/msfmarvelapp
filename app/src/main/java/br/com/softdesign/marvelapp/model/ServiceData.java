package br.com.softdesign.marvelapp.model;

import br.com.softdesign.marvelapp.model.base.BaseModel;

public class ServiceData extends BaseModel {

        private String attributionText;

        private Data data;

        private Integer code;

        public Data getData ()
        {
            return data;
        }

        public void setData (Data data)
        {
            this.data = data;
        }

        public Integer getCode ()
        {
            return code;
        }

        public void setCode (Integer code)
        {
            this.code = code;
        }

    }