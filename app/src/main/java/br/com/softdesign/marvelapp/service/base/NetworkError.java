package br.com.softdesign.marvelapp.service.base;

public class NetworkError {

    private NetworkErrorType type;
    private Throwable exception;
    private String detail;
    private String messageToUser;
    private String TAG;
    private String ORIGEM;

    public NetworkErrorType getType() {
        return type;
    }

    public void setType(NetworkErrorType type) {
        this.type = type;
    }

    public Throwable getException() {
        return exception;
    }

    public void setException(Throwable exception) {
        this.exception = exception;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getMessageToUser() {
        return messageToUser;
    }

    public void setMessageToUser(String messageToUser) {
        this.messageToUser = messageToUser;
    }

    public void setTAG(String TAG) {
        this.TAG = TAG;
    }

    public void setORIGEM(String ORIGEM) {
        this.ORIGEM = ORIGEM;
    }

    public String getTAG() {
        return TAG;
    }

    public String getORIGEM() {
        return ORIGEM;
    }
}
