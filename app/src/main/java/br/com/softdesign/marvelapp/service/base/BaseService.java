package br.com.softdesign.marvelapp.service.base;

import android.content.Context;
import android.util.ArrayMap;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import java.lang.reflect.Type;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import br.com.softdesign.marvelapp.BuildConfig;

public class BaseService {

    private static final String MARVELAPI_PRIVATE_KEY = "371d292958f8eccdd5f1178873de95ac92767237";
    private static final String MARVELAPI_PUBLIC_KEY = "81223fa37b958e61eae4ae3da753aded";

    public static final String SERVICE_CHARACTERS = "/v1/public/characters";
    public static final String SERVICE_COMICS = "/v1/public/comics";

    public static <T> void request(
            Context context,
            int method,
            String path,
            String authentication,
            String queryString,
            String body,
            final Type clazz,
            final Response.Listener<T> onResponse,
            final Response.ErrorListener onErrorResponse){

        Map<String, String> headers = new ArrayMap<>();
        if(authentication != null) {
            headers.put("Authorization", "BASIC " + authentication);
        }

        RequestQueue queue = Volley.newRequestQueue(context);
        String url = path;
        if(!url.toLowerCase().startsWith("http")) {
            url = BuildConfig.MARVELAPP_URLSERVER + path;
        }

        url += "?" + getHashParameters() + (queryString != null ? "&" + queryString : "");

        GsonRequest gsonRequest = new GsonRequest(method, url, clazz, headers,
                new Response.Listener<T>() {
                    @Override
                    public void onResponse(T response) {
                        onResponse.onResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        onErrorResponse.onErrorResponse(error);
                    }
                }
        );

        if(body != null) {
            gsonRequest.setBody(body);
        }
        gsonRequest.setShouldCache(false);
        gsonRequest.setRetryPolicy(getRetryPolicy(context));

        // Add the request to the RequestQueue.
        queue.add(gsonRequest);

    }

    public static DefaultRetryPolicy getRetryPolicy(Context context) {
        return new DefaultRetryPolicy(10000,1,1);
    }


    private static String getHashParameters() {

        String timeStamp = "1";
        String hash = md5(timeStamp + MARVELAPI_PRIVATE_KEY + MARVELAPI_PUBLIC_KEY);
        return "ts=" + timeStamp + "&apikey=" + MARVELAPI_PUBLIC_KEY +"&hash=" + hash;

    }


    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

}
