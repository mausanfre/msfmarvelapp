package br.com.softdesign.marvelapp.service.base;

public enum NetworkErrorType {

    AUTH_FAILURE,
    NO_CONNECTION,
    SSL_HANDSHAKE,
    TIMEOUT,
    UNIDENTIFIED,
    SERVER_ERROR

}
