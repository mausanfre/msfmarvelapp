package br.com.softdesign.marvelapp.viewmodel;

import android.databinding.BindingAdapter;
import android.util.Log;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import br.com.softdesign.marvelapp.model.Results;
import br.com.softdesign.marvelapp.viewmodel.base.BaseViewModel;

public class CharacterViewModel extends BaseViewModel {

    private String name;
    private String description;
    private String pictureUrl;
    private String comicsUrl;

    public CharacterViewModel(String name, String description, String pictureUrl, String comicsUrl) {
        this.name = name;
        this.description = description;
        this.pictureUrl = pictureUrl;
        this.comicsUrl = comicsUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    @BindingAdapter({"bind:imageUrl"})

    public static void loadImage(ImageView imageView, String imageUrl){
        Picasso.get().load(imageUrl).fit().into(imageView);
    }

    public String getImageUrl() {
        return pictureUrl;
    }

    public String getComicsUrl() {
        return comicsUrl;
    }

    public void setComicsUrl(String comicsUrl) {
        this.comicsUrl = comicsUrl;
    }

    public static List<CharacterViewModel> toViewModelList(List<Results> resultsList) {
        List<CharacterViewModel> viewModelList = new ArrayList<CharacterViewModel>();

        for (Results results : resultsList) {
            viewModelList.add(
                    new CharacterViewModel(
                            results.getName(),
                            results.getDescription(),
                            results.getThumbnail().getPath() + "." + results.getThumbnail().getExtension(),
                            results.getComics().getCollectionURI()
                    )
            );
        }

        return viewModelList;
    }
}
