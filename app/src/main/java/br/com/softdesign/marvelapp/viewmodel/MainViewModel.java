package br.com.softdesign.marvelapp.viewmodel;

import android.content.Context;
import android.support.annotation.NonNull;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.softdesign.marvelapp.model.ServiceData;
import br.com.softdesign.marvelapp.model.base.BaseModel;
import br.com.softdesign.marvelapp.service.CharacterService;
import br.com.softdesign.marvelapp.viewmodel.base.LoadDataCallbacks;

public class MainViewModel extends BaseModel {

    private List<LoadDataCallbacks> listeners = new ArrayList<LoadDataCallbacks>();

    private boolean loading = true;
    private long characterTotalData = 0;
    private List<CharacterViewModel> charactersList;

    public MainViewModel(Context context){
        super();
        charactersList = new ArrayList<>();
    }

    public void addListener(LoadDataCallbacks toAdd) {
        listeners.add(toAdd);
    }

    public List<CharacterViewModel> getCharactersList() {
        return charactersList;
    }

    public void setCharactersList(List<CharacterViewModel> charactersList) {
        this.charactersList = charactersList;
    }

    public boolean isLoading() {
        return loading;
    }

    public long getCharacterTotalData() {
        return characterTotalData;
    }

    public void loadCharacters(Context context, boolean nextPage) {

        if (nextPage && charactersList.size() >= getCharacterTotalData()) {
            for (LoadDataCallbacks ld : listeners)
                ld.onNoDataLoaded();
            return;
        }

        loading = false;
        Integer offset = nextPage && charactersList != null && charactersList.size() > 0 ? charactersList.size() : 0;
//        swipeRefreshLayout.setRefreshing(true);
        CharacterService.getCharacters(
                context,
                offset.toString(),
                null,
                getOnResponseServiceData(nextPage),
                getOnErrorServiceData()
        );
    }

    @NonNull
    private Response.Listener<ServiceData> getOnResponseServiceData(final boolean nextPage) {
        return new Response.Listener<ServiceData>() {
            @Override
            public void onResponse(ServiceData response) {
                List<CharacterViewModel> newDataList = CharacterViewModel.toViewModelList(Arrays.asList(response.getData().getResults()));
                characterTotalData = response.getData().getTotal().longValue();

                if (!nextPage)
                    charactersList.clear();

                charactersList.addAll(newDataList);
                for (LoadDataCallbacks ld : listeners)
                    ld.onDataLoaded(nextPage);
                loading = true;
            }
        };
    }

    @NonNull
    private Response.ErrorListener getOnErrorServiceData() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                for (LoadDataCallbacks ld : listeners)
                    ld.onError(error);
            }
        } ;
    }
}

