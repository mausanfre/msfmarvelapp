package br.com.softdesign.marvelapp.model;

import java.math.BigInteger;

import br.com.softdesign.marvelapp.model.base.BaseModel;

public class Results extends BaseModel
{
    private BigInteger id;

    private Thumbnail thumbnail;

    private String description;

    private String name;

    private String title;

    private Comics comics;

    public BigInteger getId ()
    {
        return id;
    }

    public void setId (BigInteger id)
    {
        this.id = id;
    }

    public Thumbnail getThumbnail ()
    {
        return thumbnail;
    }

    public void setThumbnail (Thumbnail thumbnail)
    {
        this.thumbnail = thumbnail;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public Comics getComics ()
    {
        return comics;
    }

    public void setComics (Comics comics)
    {
        this.comics = comics;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


}