package br.com.softdesign.marvelapp.service;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;

import java.util.Arrays;
import java.util.List;

import br.com.softdesign.marvelapp.model.Results;
import br.com.softdesign.marvelapp.model.ServiceData;
import br.com.softdesign.marvelapp.service.base.BaseService;

public class CharacterService extends BaseService {

    public static void getCharacters(
            Context context,
            String offset,
            String authentication,
            final Response.Listener<ServiceData> onResponse,
            Response.ErrorListener onErrorResponse) {

        request(
                context,
                Request.Method.GET,
                SERVICE_CHARACTERS,
                authentication,
                "limit=20&offset=" + offset,
                null,
                ServiceData.class,
                new Response.Listener<ServiceData>() {
                    @Override
                    public void onResponse(ServiceData response) {
                        onResponse.onResponse(response);
                    }
                },
                onErrorResponse
        );
    }

    public static void getComics(
            Context context,
            String offset,
            String authentication,
            String comicsUri,
            final Response.Listener<ServiceData> onResponse,
            Response.ErrorListener onErrorResponse) {

        request(
                context,
                Request.Method.GET,
                comicsUri,
                authentication,
                "limit=10&offset=" + offset,
                null,
                ServiceData.class,
                new Response.Listener<ServiceData>() {
                    @Override
                    public void onResponse(ServiceData response) {
                        onResponse.onResponse(response);
                    }
                },
                onErrorResponse
        );
    }
}
