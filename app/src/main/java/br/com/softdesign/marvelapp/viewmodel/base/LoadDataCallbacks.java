package br.com.softdesign.marvelapp.viewmodel.base;

import java.util.List;

public interface LoadDataCallbacks {
    public void onDataLoaded(boolean nextPage);
    public void onError(Exception error);
    public void onNoDataLoaded();
}
