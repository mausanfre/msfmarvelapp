package br.com.softdesign.marvelapp.viewmodel;

import android.widget.ImageView;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.softdesign.marvelapp.model.Comics;
import br.com.softdesign.marvelapp.model.Data;
import br.com.softdesign.marvelapp.model.Results;
import br.com.softdesign.marvelapp.model.ServiceData;
import br.com.softdesign.marvelapp.model.Thumbnail;

import static org.junit.Assert.*;

public class CharacterViewModelTest {

    private CharacterViewModel characterViewModel = null;
    private ServiceData serviceData = null;

    @Before
    public void setUp() {

        //Teste init
        characterViewModel = new CharacterViewModel("Spider Man", "The Famous Spider Man", "http://xxx.xxx.xxx/123456.jpg", "http://xxx.xxx.xxx/123456");

        //Teste toViewModelList
        Comics comics = new Comics();
        comics.setCollectionURI("http://gateway.marvel.com/v1/public/characters/1009146/comics");
        Thumbnail thumbnail = new Thumbnail();
        thumbnail.setPath("http://i.annihil.us/u/prod/marvel/i/mg/9/50/4ce18691cbf04");
        thumbnail.setExtension("jpg");

        Results result = new Results();
        result.setId(new BigInteger("1009146"));
        result.setName("Abomination (Emil Blonsky)");
        result.setDescription("Formerly known as Emil Blonsky, a spy of Soviet Yugoslavian origin working for the KGB, the Abomination gained his powers after receiving a dose of gamma radiation similar to that which transformed Bruce Banner into the incredible Hulk.");
        result.setName("Abomination (Emil Blonsky)");
        result.setThumbnail(thumbnail);
        result.setComics(comics);

        ArrayList<Results> resultArray = new ArrayList<Results>();
        resultArray.add(result);

        Data data = new Data();
        data.setResults(resultArray.toArray(new Results[resultArray.size()]));

        serviceData = new ServiceData();
        serviceData.setCode(200);
        serviceData.setData(data);

    }

    @Test
    public void initTest1() {

        assertNotNull(characterViewModel);

        assertEquals("Spider Man", characterViewModel.getName());
        assertEquals("The Famous Spider Man", characterViewModel.getDescription());
        assertEquals("http://xxx.xxx.xxx/123456.jpg", characterViewModel.getPictureUrl());
        assertEquals("http://xxx.xxx.xxx/123456", characterViewModel.getComicsUrl());

    }

    @Test
    public void initTest2() {

        characterViewModel.setName("Nome Teste 2");
        characterViewModel.setDescription("Description Teste 2");
        characterViewModel.setPictureUrl("Picture Teste 2");
        characterViewModel.setComicsUrl("http://yyy.yyy.yyy/54321.jpg");

        assertEquals("Nome Teste 2", characterViewModel.getName());
        assertEquals("Description Teste 2", characterViewModel.getDescription());
        assertEquals("Picture Teste 2", characterViewModel.getPictureUrl());
        assertEquals("http://yyy.yyy.yyy/54321.jpg", characterViewModel.getComicsUrl());

    }

    @Test
    public void toViewModelList() {

        Results[] results =  serviceData.getData().getResults();
        List<CharacterViewModel> list  = CharacterViewModel.toViewModelList(Arrays.asList(results));

        assertNotNull(list);
        assertEquals(list.size(), serviceData.getData().getResults().length);

        assertEquals(results[0].getName(), list.get(0).getName());
        assertEquals(results[0].getDescription(), list.get(0).getDescription());
        assertEquals(results[0].getThumbnail().getPath() + "." + results[0].getThumbnail().getExtension(), list.get(0).getPictureUrl());

    }

    @After
    public void setDown() {
        characterViewModel = null;
        serviceData = null;
    }

}