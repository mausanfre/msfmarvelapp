package br.com.softdesign.marvelapp.viewmodel;

import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.softdesign.marvelapp.model.Comics;
import br.com.softdesign.marvelapp.model.Data;
import br.com.softdesign.marvelapp.model.Results;
import br.com.softdesign.marvelapp.model.ServiceData;
import br.com.softdesign.marvelapp.model.Thumbnail;

import static org.junit.Assert.*;

public class ComicsViewModelTest {

    private ComicsViewModel comicsViewModel = null;
    ServiceData serviceData = null;

    @Before
    public void setUp() {

        comicsViewModel = new ComicsViewModel("Name Test", "Description Test", "Picture Test");

        //Teste toViewModelList
        Thumbnail thumbnail = new Thumbnail();
        thumbnail.setPath("http://i.annihil.us/u/prod/marvel/i/mg/9/b0/4c7d666c0e58a");
        thumbnail.setExtension("jpg");

        Results result = new Results();
        result.setId(new BigInteger("1590"));
        result.setTitle("Official Handbook of the Marvel Universe (2004) #9 (THE WOMEN OF MARVEL)");
        result.setThumbnail(thumbnail);

        ArrayList<Results> resultArray = new ArrayList<Results>();
        resultArray.add(result);

        Data data = new Data();
        data.setResults(resultArray.toArray(new Results[resultArray.size()]));

        serviceData = new ServiceData();
        serviceData.setCode(200);
        serviceData.setData(data);
    }

    @Test
    public void initTest1() {

        assertEquals("Name Test", comicsViewModel.getName());
        assertEquals("Description Test", comicsViewModel.getDescription());
        assertEquals("Picture Test", comicsViewModel.getPictureUrl());

    }

    @Test
    public void initTest2() {

        comicsViewModel.setName("Name Test");
        comicsViewModel.setDescription("Description Test");
        comicsViewModel.setPictureUrl("Picture Test");

        assertEquals("Name Test", comicsViewModel.getName());
        assertEquals("Description Test", comicsViewModel.getDescription());
        assertEquals("Picture Test", comicsViewModel.getPictureUrl());

    }

    @Test
    public void toViewModelList() {

        Results[] results =  serviceData.getData().getResults();
        List<ComicsViewModel> list  = ComicsViewModel.toViewModelList(Arrays.asList(results));

        assertNotNull(list);
        assertEquals(list.size(), serviceData.getData().getResults().length);

        assertEquals(results[0].getTitle(), list.get(0).getName());
        assertEquals(results[0].getDescription(), list.get(0).getDescription());
        assertEquals(results[0].getThumbnail().getPath() + "." + results[0].getThumbnail().getExtension(), list.get(0).getPictureUrl());

    }

}