package br.com.softdesign.marvelapp.viewmodel;

import android.content.Context;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DetailViewModelTest {

    private DetailViewModel detailViewModel;

    @Before
    public void setUp() {
        detailViewModel = new DetailViewModel("Spider Man", "The Famous Spider Man", "http://xxx.xxx.xxx/123456.jpg", "http://xxx.xxx.xxx/123456");
    }

    @Test
    public void initTest() {

        assertNotNull(detailViewModel);

        assertEquals("Spider Man", detailViewModel.getName());
        assertEquals("The Famous Spider Man", detailViewModel.getDescription());
        assertEquals("http://xxx.xxx.xxx/123456.jpg", detailViewModel.getPictureUrl());
        assertEquals("http://xxx.xxx.xxx/123456", detailViewModel.getComicsUrl());

    }
}