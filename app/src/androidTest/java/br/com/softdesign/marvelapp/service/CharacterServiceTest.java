package br.com.softdesign.marvelapp.service;

import android.content.Context;
import android.support.test.InstrumentationRegistry;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.softdesign.marvelapp.model.ServiceData;

import static org.junit.Assert.*;

public class CharacterServiceTest {

    private Context instrumentationCtx;
    ServiceData serviceData = null;

    @Before
    public void setUp() throws Exception {
        instrumentationCtx = InstrumentationRegistry.getContext();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getCharacters() throws Exception {

        final Object syncObject = new Object();

        CharacterService.getCharacters(
                instrumentationCtx,
                "0",
                null,
                new Response.Listener<ServiceData>() {
                    @Override
                    public void onResponse(ServiceData response) {
                        assertNotNull(response);
                        assertEquals(20, response.getData().getResults().length);

                        synchronized (syncObject){
                            syncObject.notify();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        assertNotNull(serviceData);
                        synchronized (syncObject){
                            syncObject.notify();
                        }

                    }
                }
        );

        synchronized (syncObject){
            syncObject.wait();
        }
    }

    @Test
    public void getComics() throws Exception {

        final Object syncObject = new Object();

        CharacterService.getComics(
                instrumentationCtx,
                "0",
                null,
                "http://gateway.marvel.com/v1/public/characters/1011334/comics",
                new Response.Listener<ServiceData>() {
                    @Override
                    public void onResponse(ServiceData response) {
                        assertNotNull(response);
                        assertEquals(10, response.getData().getResults().length);
                        synchronized (syncObject){
                            syncObject.notify();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        assertNotNull(serviceData);
                        synchronized (syncObject){
                            syncObject.notify();
                        }

                    }
                }
        );

        synchronized (syncObject){
            syncObject.wait();
        }
    }
}