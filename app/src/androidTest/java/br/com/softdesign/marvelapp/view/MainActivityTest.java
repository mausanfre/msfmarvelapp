package br.com.softdesign.marvelapp.view;

import android.app.Activity;
import android.support.test.rule.ActivityTestRule;
import android.support.v7.widget.RecyclerView;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import br.com.softdesign.marvelapp.R;
import br.com.softdesign.marvelapp.viewmodel.CharacterViewModel;
import br.com.softdesign.marvelapp.viewmodel.MainViewModel;

import static org.junit.Assert.*;

public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<MainActivity>(MainActivity.class);

    MainActivity activity;

    @Before
    public void setUp() throws Exception {

        activity = activityTestRule.getActivity();
    }

    @Test
    public void testUI(){

        assertNotNull(activity.findViewById(R.id.swipeRefreshLayout));
        RecyclerView recyclerView = activity.findViewById(R.id.characterRecyclerView);
        assertNotNull(recyclerView);
        assertTrue(recyclerView.isShown());

    }

    @Test
    public void testLoadCharactersMethod() throws Exception {

        activity.mainViewModel.loadCharacters(activity, false);

        Thread.sleep(5000);

        assertEquals("Load characters fail", 20, activity.recyclerView.getAdapter().getItemCount());
    }


    @After
    public void tearDown() throws Exception {
        activity.finish();
    }

}