package br.com.softdesign.marvelapp.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.v7.widget.RecyclerView;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import br.com.softdesign.marvelapp.R;
import br.com.softdesign.marvelapp.viewmodel.CharacterViewModel;
import br.com.softdesign.marvelapp.viewmodel.DetailViewModel;

import static org.junit.Assert.*;


public class DetailViewActivityTest {

    @Rule
    public ActivityTestRule<DetailActivity> activityTestRule = new ActivityTestRule<DetailActivity>(DetailActivity.class);

    DetailActivity activity;

    @Before
    public void setUp() throws Exception {
        activity = activityTestRule.getActivity();
    }

    @Test
    public void testUI(){


        RecyclerView recyclerView = activity.findViewById(R.id.comicsRecyclerView);
        assertNotNull(recyclerView);
        assertTrue(recyclerView.isShown());

        assertNotNull(activity.findViewById(R.id.comicsProgressBar));
        assertNotNull(activity.findViewById(R.id.pictureImageView));
        assertNotNull(activity.findViewById(R.id.nameTextView));
        assertNotNull(activity.findViewById(R.id.descriptionTextView));


    }

    @After
    public void tearDown() throws Exception {
        activity.finish();
    }

}
